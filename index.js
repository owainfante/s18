/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

		Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

		Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

const addition = function(number1, number2) {
    let addedNumber = number1 + number2
    console.log("Displayed sum of " + number1 + " and " + number2);
    console.log(addedNumber)
}

addition(5,15)

const subtract = function(number1, number2) {
    let subtractedNumber = number1 - number2
    console.log("Displayed difference of " + number1 + " and " + number2)
    console.log(subtractedNumber)
}

subtract(20,5)

const multiply = function(number1, number2) {
    let multipliedNumber = number1 * number2
    console.log("The product of " + number1 + " and " + number2)
    return multipliedNumber
}

let product = multiply(50,10)
console.log(product)

const divide = function(number1, number2) {
    let dividedNumber = number1 / number2
    console.log("The quotient of " + number1 + " and " + number2);
    return dividedNumber
}

let quotient = divide(50,10)
console.log(quotient)

const circleAreaFormula = function(number) {
    let area =  Math.PI * Math.pow(number, 2);
    console.log("The result of the area of a circle with " + number + " radius");
    return area
}

let circleArea= circleAreaFormula(15)
console.log(circleArea)

const averageChecker = function(num1,num2,num3,num4){
    let average = (num1 + num2 + num3 + num4)/4
    console.log("The average of " + num1 + ", " + num2 + ", " + num3 + ", " + num4 + ":");
    return average
}

let averageVar = averageChecker(20,40,60,80)
console.log(averageVar)

const isPassed = function(score, total) {
    let scorePercentage = (score/total) * 100
    let passingScore = 75
    let isPassed = scorePercentage >=passingScore 
    console.log("is " + score + "/" +total + " a passing score?")
    return isPassed
}

let isPassingScore = isPassed(38,50)
console.log(isPassingScore)
